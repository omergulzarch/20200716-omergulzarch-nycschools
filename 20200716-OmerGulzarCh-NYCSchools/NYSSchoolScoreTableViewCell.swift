//
//  NYSSchoolScoreTableViewCell.swift
//  20200716-OmerGulzarCh-NYCSchools
//
//  Created by Omer Gulzar on 7/18/20.
//  Copyright © 2020 JPMC. All rights reserved.
//

import UIKit

class NYSSchoolScoreTableViewCell: UITableViewCell {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var valueLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
