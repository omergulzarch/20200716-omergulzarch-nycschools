//
//  NYSSchoolDetailViewController.swift
//  20200716-OmerGulzarCh-NYCSchools
//
//  Created by Omer Gulzar on 7/15/20.
//  Copyright © 2020 JPMC. All rights reserved.
//

import UIKit
import MapKit

class NYSSchoolDetailViewController: UIViewController, HttpGetDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var detailTableView: UITableView!
    
    var satTakers:String = ""
    var maths:String = ""
    var reading:String = ""
    var writing:String = ""
    
    var schoolName:String = ""
    var schooldbn:String = ""
    
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    
    var school_email:String = ""
    var phone_number:String = ""
    var fax_number:String = ""
    var website:String = ""
    var total_students:String = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Set title
        self.title = schoolName
        
        // Set tableview delegate
        self.detailTableView.dataSource = self
        self.detailTableView.delegate = self
        
        // Set tableview hidden
        self.detailTableView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // Add annotation to map
        if self.latitude != 0.0 && self.longitude != 0.0 {
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
            annotation.title = self.schoolName
            self.mapView.addAnnotation(annotation)
            
            let region = MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
            self.mapView.setRegion(region, animated: true)
        }
        
        // Fetch Data for NYS schools
        let detailsDataFetch = HttpGet()
        detailsDataFetch.delegate = self
        
        if !detailsDataFetch.get(urlString: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(self.schooldbn)") {
            
            let alertController = UIAlertController(title: "Error Creating URL", message: "Error creating URL object. Please try again.", preferredStyle: .alert)
            
            let okayAction = UIAlertAction(title: "OK", style: .cancel) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(okayAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func directionsBtnTouchUp(_ sender: Any) {
        
        // Open in apple maps
        let latitude: CLLocationDegrees = self.latitude
        let longitude: CLLocationDegrees = self.longitude

        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.schoolName
        mapItem.openInMaps(launchOptions: options)
    }
    
    func httpDidGetData(response: String) {
        
        if let detailInfoArray = Utilities.jsonToDict(jString: response) {
            
            var notFound:Bool = true
            
            for i in 0..<detailInfoArray.count{
                
                if let detailInfo = detailInfoArray.object(at: i) as? NSDictionary {
                    
                    if detailInfo.object(forKey: "dbn") as? String == self.schooldbn {
                        
                        notFound = false
                        
                        self.satTakers = detailInfo.object(forKey: "num_of_sat_test_takers") as? String ?? "NO DATA FOUND"
                        self.maths = detailInfo.object(forKey: "sat_math_avg_score") as? String ?? "NO DATA FOUND"
                        self.reading = detailInfo.object(forKey: "sat_critical_reading_avg_score") as? String ?? "NO DATA FOUND"
                        self.writing = detailInfo.object(forKey: "sat_writing_avg_score") as? String ?? "NO DATA FOUND"
                        
                        DispatchQueue.main.async {
                            
                            self.detailTableView.reloadData()
                        }
                    }
                }
            }
            
            if notFound {
                
                self.satTakers = "NO DATA FOUND"
                self.maths = "NO DATA FOUND"
                self.reading = "NO DATA FOUND"
                self.writing = "NO DATA FOUND"
            }
            
        } else {
            
            let alertController = UIAlertController(title: "Error Parsing JSON", message: "Error parsing JSON. Please try again later.", preferredStyle: .alert)
            
            let okayAction = UIAlertAction(title: "OK", style: .cancel) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(okayAction)
            self.present(alertController, animated: true, completion: nil)
            
            self.satTakers = "---"
            self.maths = "---"
            self.reading = "---"
            self.writing = "---"
        }
        
        DispatchQueue.main.async {
            
            self.detailTableView.reloadData()
            self.detailTableView.isHidden = false
        }
    }
    
    func httpDidFailWithError(error: String) {
        
    }
    
    // MARK: - Tableview delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            return "Average Scores"
        } else {
            return "School Information"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 4
        } else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "scoreCell", for: indexPath)
            
            if let cell = cell as? NYSSchoolScoreTableViewCell {
                
                switch indexPath.row {
                case 0:
                    cell.titleLbl.text = "Total SAT Takers"
                    cell.valueLbl.text = self.satTakers
                case 1:
                    cell.titleLbl.text = "Avg Maths Score"
                    cell.valueLbl.text = self.maths
                case 2:
                    cell.titleLbl.text = "Avg Reading Score"
                    cell.valueLbl.text = self.reading
                case 3:
                    cell.titleLbl.text = "Avg Writing Score"
                    cell.valueLbl.text = self.writing
                
                default:
                    cell.titleLbl.text = ""
                    cell.valueLbl.text = ""
                }
            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath)
            
            if let cell = cell as? NYSSchoolDetailTableViewCell {
                
                switch indexPath.row {
                case 0:
                    cell.titleLbl.text = "Email"
                    cell.valueLbl.text = self.school_email
                case 1:
                    cell.titleLbl.text = "Phone"
                    cell.valueLbl.text = self.phone_number
                case 2:
                    cell.titleLbl.text = "Fax"
                    cell.valueLbl.text = self.fax_number
                case 3:
                    cell.titleLbl.text = "Website"
                    cell.valueLbl.text = self.website
                case 4:
                    cell.titleLbl.text = "Students"
                    cell.valueLbl.text = self.total_students
                
                default:
                    cell.titleLbl.text = ""
                    cell.valueLbl.text = ""
                }
            }
            
            return cell
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
