//
//  Utilities.swift
//  20200716-OmerGulzarCh-NYCSchools
//
//  Created by Omer Gulzar on 7/15/20.
//  Copyright © 2020 JPMC. All rights reserved.
//

import Foundation

class Utilities {
    
    static func jsonToDict(jString:String) -> NSArray? {
        
        do {
            
            guard let jStringData = jString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            let json = try JSONSerialization.jsonObject(with: jStringData, options: []) as  AnyObject
            if let jsonArray = json as? NSArray {
                return jsonArray
            } else {
                return nil
            }
        
        } catch _ as NSError {
            return nil
        }
    }
    
}
