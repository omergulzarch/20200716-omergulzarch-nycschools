//
//  HttpGet.swift
//  20200716-OmerGulzarCh-NYCSchools
//
//  Created by Omer Gulzar on 7/15/20.
//  Copyright © 2020 JPMC. All rights reserved.
//

import Foundation

protocol HttpGetDelegate {
    func httpDidGetData(response:String)
    func httpDidFailWithError(error:String)
}

class HttpGet : NSObject {
    
    var delegate: HttpGetDelegate?
    /*
    func getData(urlString: String, completion: (String) -> Void, onFailure: () -> Void) {
        
        guard let url = URL(string: urlString) else {
            onFailure()
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                onFailure()
                return
            }
            
            guard let data = data else {
                onFailure()
                return
            }
            
            let value = String(bytes: data, encoding: String.Encoding.utf8)
            guard let value2 = value else {
                onFailure()
                return
            }
            
            completion(value2)
            
        }.resume()
    }
    */
    
    func get(urlString:String) -> Bool {
        
        guard let url = URL(string: urlString) else {
            return false
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                self.delegate?.httpDidFailWithError(error: error?.localizedDescription ?? "")
            }
            
            guard let data = data else {
                return
            }
            
            let value = String(bytes: data, encoding: String.Encoding.utf8)
            guard let value2 = value else {
                return
            }
            
            self.delegate?.httpDidGetData(response: value2)
            
        }.resume()
        
        return true
    }
}
