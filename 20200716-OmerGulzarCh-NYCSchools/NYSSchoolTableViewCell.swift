//
//  NYSSchoolTableViewCell.swift
//  20200716-OmerGulzarCh-NYCSchools
//
//  Created by Omer Gulzar on 7/15/20.
//  Copyright © 2020 JPMC. All rights reserved.
//

import UIKit

class NYSSchoolTableViewCell: UITableViewCell {

    @IBOutlet var schoolNameLbl: UILabel!
    @IBOutlet var neighborhoodLbl: UILabel!
    @IBOutlet var cityLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
