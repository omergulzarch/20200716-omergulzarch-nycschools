//
//  NYSSchoolsViewController.swift
//  20200716-OmerGulzarCh-NYCSchools
//
//  Created by Omer Gulzar on 7/15/20.
//  Copyright © 2020 JPMC. All rights reserved.
//

import UIKit

extension NYSSchoolsViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    
    let searchBar = searchController.searchBar
    filterContentForSearchText(searchBar.text ?? "")
  }
}

class NYSSchoolsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, HttpGetDelegate {
    
    @IBOutlet var loadingLbl: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var schoolNamesTableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    var searchTxt:String {
        return searchController.searchBar.text ?? ""
    }
    
    private var _schoolsData:NSArray?
    private var _filteredData:NSArray?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Set title
        self.title = "NYS School"
        
        // Set bar button to refresh
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Refresh", style: .plain, target: self, action: #selector(refreshView))
        
        // Set tableview delegate and datasource
        self.schoolNamesTableView.delegate = self
        self.schoolNamesTableView.dataSource = self
        
        // Setup searchbar
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Search Schools"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        // Call API to get data
        refreshView()
    }
    
    @objc
    func refreshView() {
        
        // Initialize the UI
        self.schoolNamesTableView.isHidden = true
        self.activityIndicator.startAnimating()
        self.loadingLbl.isHidden = false
        
        // Fetch Data for NYS schools
        let schoolsDataFetch = HttpGet()
        schoolsDataFetch.delegate = self
        
        if !schoolsDataFetch.get(urlString: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") {
            
            let alertController = UIAlertController(title: "Error Creating URL", message: "Error creating URL object. Please try again by tapping on the refresh button in the upper right corner.", preferredStyle: .alert)
            
            let okayAction = UIAlertAction(title: "OK", style: .cancel) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(okayAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func httpDidGetData(response: String) {
        
        if let schoolsArray = Utilities.jsonToDict(jString: response) {
            
            _schoolsData = NSArray.init(array: schoolsArray.sortedArray(using: [NSSortDescriptor.init(key: "school_name", ascending: true)]))
            
            DispatchQueue.main.async {
                
                self.schoolNamesTableView.reloadData()
                
                self.schoolNamesTableView.isHidden = false
                self.loadingLbl.isHidden = true
                self.activityIndicator.stopAnimating()
            }
            
        } else {
            
            let alertController = UIAlertController(title: "Error Parsing JSON", message: "Error parsing JSON. Please try again by tapping on the refresh button in the upper right corner.", preferredStyle: .alert)
            
            let okayAction = UIAlertAction(title: "OK", style: .cancel) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(okayAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func httpDidFailWithError(error: String) {
        
        let alertController = UIAlertController(title: "Error fetching data", message: "Error fetching data from the server. Please try again by tapping on the refresh button in the upper right corner.", preferredStyle: .alert)
        
        let okayAction = UIAlertAction(title: "OK", style: .cancel) { (action:UIAlertAction) in
            
        }
        
        alertController.addAction(okayAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        _filteredData = _schoolsData?.filtered(using: NSPredicate.init(format: "(school_name CONTAINS[c] %@)", searchText.lowercased())) as NSArray?
        
        self.schoolNamesTableView.reloadData()
    }
    
    func addBoldText(fullString: String, boldPartOfString: String, baseFont: UIFont, boldFont: UIFont) -> NSAttributedString {
        
        let baseFontAttribute = [NSAttributedString.Key.font: baseFont, NSAttributedString.Key.foregroundColor: UIColor.gray]
        let boldFontAttribute = [NSAttributedString.Key.font : boldFont, NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let attributedString = NSMutableAttributedString(string: fullString, attributes: baseFontAttribute)
        
        attributedString.addAttributes(boldFontAttribute, range: NSRange(fullString.lowercased().range(of: boldPartOfString.lowercased()) ?? fullString.startIndex..<fullString.endIndex, in: fullString))
        
        return attributedString
    }
    
    // MARK: - Tableview delegates
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering {
          return _filteredData?.count ?? 0
        }
        
        return _schoolsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolNameCell", for: indexPath)
        
        if let cell = cell as? NYSSchoolTableViewCell {
            
            if isFiltering {
                
                if let cellDictionay = _filteredData?.object(at: indexPath.row) as? NSDictionary {
                    
                    let schoolNameTxt = (cellDictionay.object(forKey: "school_name") as? String ?? "")
                    let neighborhoodTxt = (cellDictionay.object(forKey: "neighborhood") as? String ?? "")
                    let cityTxt = (cellDictionay.object(forKey: "city") as? String ?? "")
                    
                    cell.schoolNameLbl.attributedText = addBoldText(fullString: schoolNameTxt, boldPartOfString: self.searchTxt, baseFont: UIFont.boldSystemFont(ofSize: 20.0), boldFont: UIFont.boldSystemFont(ofSize: 20.0))
                    
                    cell.neighborhoodLbl.text = neighborhoodTxt
                    cell.cityLbl.text = cityTxt
                }
                
            } else {
                
                if let cellDictionay = _schoolsData?.object(at: indexPath.row) as? NSDictionary {
                    
                    let schoolNameTxt = (cellDictionay.object(forKey: "school_name") as? String ?? "")
                    let neighborhoodTxt = (cellDictionay.object(forKey: "neighborhood") as? String ?? "")
                    let cityTxt = (cellDictionay.object(forKey: "city") as? String ?? "")
                    
                    cell.schoolNameLbl.attributedText = NSMutableAttributedString(string: schoolNameTxt, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 20.0)])
                    
                    cell.neighborhoodLbl.text = neighborhoodTxt
                    cell.cityLbl.text = cityTxt
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "schoolDetail", sender: nil)
        
        self.schoolNamesTableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "schoolDetail" {
        
            guard let indexPath = self.schoolNamesTableView.indexPathForSelectedRow else {
                return
            }
            
            if let detailViewCtrl = segue.destination as? NYSSchoolDetailViewController {
                
                if isFiltering {
                    
                    if let cellDictionay = _filteredData?.object(at: indexPath.row) as? NSDictionary {
                        detailViewCtrl.schoolName = cellDictionay.object(forKey: "school_name") as? String ?? ""
                        detailViewCtrl.schooldbn = cellDictionay.object(forKey: "dbn") as? String ?? ""
                        
                        detailViewCtrl.school_email = cellDictionay.object(forKey: "school_email") as? String ?? ""
                        detailViewCtrl.phone_number = cellDictionay.object(forKey: "phone_number") as? String ?? ""
                        detailViewCtrl.fax_number = cellDictionay.object(forKey: "fax_number") as? String ?? ""
                        detailViewCtrl.website = cellDictionay.object(forKey: "website") as? String ?? ""
                        detailViewCtrl.total_students = cellDictionay.object(forKey: "total_students") as? String ?? ""
                        
                        detailViewCtrl.latitude = Double(cellDictionay.object(forKey: "latitude") as? String ?? "") ?? 0.0
                        detailViewCtrl.longitude = Double(cellDictionay.object(forKey: "longitude") as? String ?? "") ?? 0.0
                    }
                    
                } else {
                    
                    if let cellDictionay = _schoolsData?.object(at: indexPath.row) as? NSDictionary {
                        detailViewCtrl.schoolName = cellDictionay.object(forKey: "school_name") as? String ?? ""
                        detailViewCtrl.schooldbn = cellDictionay.object(forKey: "dbn") as? String ?? ""
                        
                        detailViewCtrl.school_email = cellDictionay.object(forKey: "school_email") as? String ?? ""
                        detailViewCtrl.phone_number = cellDictionay.object(forKey: "phone_number") as? String ?? ""
                        detailViewCtrl.fax_number = cellDictionay.object(forKey: "fax_number") as? String ?? ""
                        detailViewCtrl.website = cellDictionay.object(forKey: "website") as? String ?? ""
                        detailViewCtrl.total_students = cellDictionay.object(forKey: "total_students") as? String ?? ""
                        
                        detailViewCtrl.latitude = Double(cellDictionay.object(forKey: "latitude") as? String ?? "") ?? 0.0
                        detailViewCtrl.longitude = Double(cellDictionay.object(forKey: "longitude") as? String ?? "") ?? 0.0
                    }
                }
            }
        }
    }
    
}
