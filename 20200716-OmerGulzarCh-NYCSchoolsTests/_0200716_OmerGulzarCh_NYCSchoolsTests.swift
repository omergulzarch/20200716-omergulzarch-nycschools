//
//  _0200716_OmerGulzarCh_NYCSchoolsTests.swift
//  20200716-OmerGulzarCh-NYCSchoolsTests
//
//  Created by Omer Gulzar on 7/17/20.
//  Copyright © 2020 JPMC. All rights reserved.
//

import XCTest

class _0200716_OmerGulzarCh_NYCSchoolsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testMainView() throws {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let schoolsVC = storyboard.instantiateViewController(withIdentifier: "NYCSchoolList")
        
        XCTAssertNotNil(schoolsVC, "Unable to instantiate Schools View Controller")
    }
    
    func testDetailView() throws {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailsVC = storyboard.instantiateViewController(withIdentifier: "NYSSchoolDetail")
        
        XCTAssertNotNil(detailsVC, "Unable to instantiate Details View Controller")
    }
    
    func testSchoolsUrl() throws {
        
        let promise = expectation(description: "Get response")
        
        if let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") {
            
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if error == nil {
                    promise.fulfill()
                } else {
                    XCTAssertNil(error)
                }
                
            }.resume()
            
            wait(for: [promise], timeout: 15)
            
        } else {
            
            XCTAssertThrowsError("Unable to create URL")
        }
    }
    
    func testScoresUrl() throws {
        
        let promise = expectation(description: "Get response")
        
        if let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") {
            
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if error == nil {
                    promise.fulfill()
                } else {
                    XCTAssertNil(error)
                }
                
            }.resume()
            
            wait(for: [promise], timeout: 15)
            
        } else {
            
            XCTAssertThrowsError("Unable to create URL")
        }
    }
}
